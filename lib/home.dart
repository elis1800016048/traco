import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:traco/home/Beranda.dart';
import 'package:traco/Riwayat.dart';
import 'package:traco/informasi.dart';

import 'package:traco/profile.dart';


class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int currentTab = 0;
  final List<Widget> screens = [
    Beranda(),
    Informasi(),
    Riwayat(),
    Profile()
  ];
  final PageStorageBucket bucket = PageStorageBucket();
  Widget currentScreen = Beranda();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageStorage(
        child: currentScreen,
          bucket: bucket,
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Color(0xff0BC734),
        child: Icon(Icons.add_circle),
        onPressed: () {
        Navigator.of(context).pushNamed('/jual');
         }),
      
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        notchMargin: 0,
        child: Container(
          height: 60,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget> [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              MaterialButton(
                minWidth: 80,
                onPressed: () {
                  setState(() {
                    currentScreen = Beranda();
                    currentTab = 0;
                  });
                },
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.home,
                      color: currentTab == 0 ? Color(0xff0BC734) : Colors.grey,
                    ),
                    
                  ],
                ),
              ),
              MaterialButton(
                minWidth: 80,
                onPressed: () {
                  setState(() {
                    currentScreen = Informasi();
                    currentTab = 1;
                  });
                },
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.menu_book,
                      color: currentTab == 1 ? Color(0xff0BC734) : Colors.grey,
                    ),
                    
                  ],
                ),
              )
            ],
          ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  MaterialButton(
                    minWidth: 80,
                    onPressed: () {
                      setState(() {
                        currentScreen = Riwayat();
                        currentTab = 2;
                      });
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.book_outlined,
                          color: currentTab == 2 ? Color(0xff0BC734) : Colors.grey,
                        ),
                        
                      ],
                    ),
                  ),
                  MaterialButton(
                    minWidth: 80,
                    onPressed: () {
                      setState(() {
                        currentScreen = Profile();
                        currentTab = 3;
                      });
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.person,
                          color: currentTab == 3 ? Color(0xff0BC734) : Colors.grey,
                        ),
                        
                      ],
                    ),
                  )
                ],
              )
              ],
        ),
      ),
    ),);
  }
}
