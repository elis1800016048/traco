import 'package:flutter/material.dart';

class DetailBotol extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var stitle = new TextStyle(fontFamily: 'Nexa Bold', fontSize: 12);
    var ssubtitle = new TextStyle(fontFamily: 'Nexa Light', fontSize: 11);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.white,
          centerTitle: true,
          elevation: 0,
          leading: IconButton(
                icon: Icon(Icons.arrow_back_ios,
                color: Colors.black),
                onPressed: () {
                  Navigator.of(context).pop();
                }),
          title: Text(
            'Informasi',
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
              fontFamily: 'Nexa Bold',
              fontSize: 18,
            ),
          ),
        ),
        body: ListView(
          children: [
            ListTile(
                title: Text("Pet Botol Warna", style: stitle),
                subtitle: Text("Rp 500 - Rp 1500", style: ssubtitle),
                
                leading: Container(
            width: 70.0,
            height: 70.0,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              color: Color(0xff1FAEFF),
            ),
            child: Image(image: AssetImage('/images/abotol.png'))
            ),
            ),

             ListTile(
                title: Text("Pet Botol", style: stitle),
                subtitle: Text("Rp 700 - Rp 2000", style: ssubtitle),
                
                leading: Container(
            width: 70.0,
            height: 70.0,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              color: Color(0xffFFE91F),
            ),
            child: Image(image: AssetImage('/images/apet.png'))
            ),
            ),

             ListTile(
                title: Text("Galon Air", style: stitle),
                subtitle: Text("Rp 2000 - Rp 3500", style: ssubtitle),
                
                leading: Container(
            width: 70.0,
            height: 70.0,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              color: Color(0xff1FFFD7),
            ),
            child: Image(image: AssetImage('/images/agalon.png'))
            ),
            ),

             ListTile(
                title: Text("PP Gelas", style: stitle),
                subtitle: Text("Rp 700 - Rp 1900", style: ssubtitle),
                
                leading: Container(
            width: 70.0,
            height: 70.0,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              color: Color(0xff1FFF78),
            ),
            child: Image(image: AssetImage('/images/appgelas.png'))
            ),
            ),

             ListTile(
                title: Text("PVC", style: stitle),
                subtitle: Text("Rp 500 - Rp 1500", style: ssubtitle),
                
                leading: Container(
            width: 70.0,
            height: 70.0,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              color: Color(0xff965EF0),
            ),
            child: Image(image: AssetImage('/images/apvc.png'))
            ),
            ),
            
            ListTile(
                title: Text("Plastik Lembar Non Alumunium Foil", style: stitle),
                subtitle: Text("Rp 500 - Rp 1500", style: ssubtitle),
                
                leading: Container(
            width: 70.0,
            height: 70.0,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              color: Color(0xffF590DF),
            ),
            child: Image(image: AssetImage('/images/alembar.png'))
            ),
            ),
          ],
        ),
      ),
    );
  }
}