import 'package:flutter/material.dart';

class Verification extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            leading: IconButton(
                icon: Icon(Icons.arrow_back_ios),
                onPressed: () {
                  Navigator.of(context).pop();
                }),
            iconTheme: IconThemeData(
              color: Colors.black, //change your color here
            ),
            backgroundColor: Colors.white),
        body: ListView(children: <Widget>[
          SizedBox(height: 30.0),
          Container(
            child: Stack(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(
                      top: 50.0, left: 50.0, bottom: 0.0, right: 0.0),
                  child: Text('Verifikasi Akun',
                      style: TextStyle(
                          fontFamily: 'Nexa Bold',
                          fontWeight: FontWeight.bold,
                          fontSize: 40.0)),
                ),
              ],
            ),
          ),
          SizedBox(height: 50.0),
          Container(
              padding: EdgeInsets.only(top: 0.0, left: 50.0, right: 50.0),
              child: Column(
                children: <Widget>[
                  TextField(
                    decoration: InputDecoration(
                        labelText: 'Masukkan nomor telepon',
                        labelStyle: TextStyle(
                            fontFamily: 'Nexa Light',
                            fontSize: 12,
                            color: Colors.grey),
                        // hintText: 'EMAIL',
                        // hintStyle: ,
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.green))),
                  ),
                  SizedBox(height: 50.0),
                  Container(
                      height: 45.0,
                      width: 231,
                      child: Material(
                        borderRadius: BorderRadius.circular(20.0),
                        shadowColor: Colors.greenAccent,
                        color: Colors.green,
                        elevation: 7.0,
                        child: GestureDetector(
                          onTap: () {
                            Navigator.of(context).pushNamed('/afterverif');
                          },
                          child: Center(
                            child: Text(
                              'Kirim Kode OTP',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15,
                                  fontFamily: 'Nexa Bold'),
                            ),
                          ),
                        ),
                      )),
                ],
              )),
        ]));
  }
}
